﻿using LiveStart.Library;
using LiveStart.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LiveStart.Contracts
{
    public class NoteContract
    {
        private string dataFile;
        public NoteContract()
        {
            string directory = System.IO.Path.GetDirectoryName(System.Diagnostics.Process.GetCurrentProcess().MainModule.FileName);
            dataFile = directory + "\\NoteData.xml";
        }
        public NoteCollection GetList()
        {
            NoteCollection collection = new NoteCollection();
            if (File.Exists(dataFile))
            {
                collection = XMLSerializer.Deserialize(typeof(NoteCollection), dataFile) as NoteCollection;
            }
            return collection;
        }
        public void Save(NoteCollection collection)
        {
            XMLSerializer.Serializer(typeof(NoteCollection), collection, dataFile);
        }
        public NoteCollection GetNew(NoteCollection collection,Note model)
        {
            if (model == null) return collection;
            model.ID = collection.Count;
            collection.Add(model);
            return collection;
        }
        public NoteCollection Delete(NoteCollection collection, Note model)
        {

            Note del = collection.FirstOrDefault(p => p.Name == model.Name);
            if (del != null)
            {
                collection.Remove(del);
                Save(collection);                
            }
            return collection;
        }
    }
}
