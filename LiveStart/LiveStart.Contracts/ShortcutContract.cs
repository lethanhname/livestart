﻿using LiveStart.Library;
using LiveStart.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LiveStart.Contracts
{
    public class ShortcutContract
    {
        private string dataFile;
        public ShortcutContract()
        {
            string directory = System.IO.Path.GetDirectoryName(System.Diagnostics.Process.GetCurrentProcess().MainModule.FileName);
            dataFile = directory + "\\AppData.xml";
        }
        public ShortcutCollection GetList()
        {
            ShortcutCollection collection = new ShortcutCollection();
            if (File.Exists(dataFile))
            {
                collection = XMLSerializer.Deserialize(typeof(ShortcutCollection), dataFile) as ShortcutCollection;
            }
            return collection;
        }
        public void Save(ShortcutCollection collection)
        {
            XMLSerializer.Serializer(typeof(ShortcutCollection), collection, dataFile);
        }
        public ShortcutCollection GetNew(ShortcutCollection collection,Shortcut model)
        {
            if (model == null) return collection;
            model.ID = collection.Count;
            collection.Add(model);
            return collection;
        }
        public ShortcutCollection Delete(ShortcutCollection collection, Shortcut model)
        {

            Shortcut del = collection.FirstOrDefault(p => p.Location == model.Location
                                                     && p.Name == model.Name);
            if (del != null)
            {
                collection.Remove(del);
                Save(collection);
            }
            return collection;
        }
    }
}
