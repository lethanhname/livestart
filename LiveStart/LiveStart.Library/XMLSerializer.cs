﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace LiveStart.Library
{
    public class XMLSerializer
    {
        public static void Serializer(Type type,object sender,string filePath)
        {
            try
            {
                using (TextWriter reader = new StreamWriter(filePath))
                {
                    (new XmlSerializer(type)).Serialize(reader, sender);
                }

            }
            catch (Exception e)
            {
                throw e;
            }
        }
        public static object Deserialize(Type type,string filePath)
        {
            object myXmlClass = null;
            try
            {
                using (TextReader reader = new StreamReader(filePath))
                {
                    myXmlClass = (object)(new XmlSerializer(type)).Deserialize(reader);                    
                }

            }
            catch (Exception e)
            {
                throw e;
            }
            return myXmlClass;

        }
    }
}
