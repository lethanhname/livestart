﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace LiveStart.Library
{
    public class ProcessStart
    {
        public static void StartRemote(string ip, string Arguments)
        {
            Process rdcProcess = new Process();
            rdcProcess.StartInfo.FileName = Environment.ExpandEnvironmentVariables(@"%SystemRoot%\system32\cmdkey.exe");
            rdcProcess.StartInfo.Arguments = Arguments;
            rdcProcess.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
            rdcProcess.Start();
            rdcProcess.StartInfo.FileName = Environment.ExpandEnvironmentVariables(@"%SystemRoot%\system32\mstsc.exe");
            rdcProcess.StartInfo.Arguments = "/v " + ip; // ip or name of computer to connect
            if (rdcProcess.Start())
            {
                Thread.Sleep(8000);
                //delete key
                rdcProcess.StartInfo.FileName = Environment.ExpandEnvironmentVariables(@"%SystemRoot%\system32\cmdkey.exe");
                rdcProcess.StartInfo.Arguments = "/delete:TERMSRV/" + ip;
                rdcProcess.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
                rdcProcess.Start();
            }

        }
    }
}
