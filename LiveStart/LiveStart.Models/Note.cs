﻿using LiveStart.Models.Base;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace LiveStart.Models
{
    public class Note : BaseObject
    {
        //ID
        [XmlElement(ElementName = "ID")]
        public int ID
        {
            get { return this.id; }
            set
            {
                if (base.Compare(id, value) != 0)
                { this.id = value; OnPropertyChanged("ID"); }
            }
        }
        private int id;

        [XmlElement(ElementName = "Name")]
        public string Name
        {
            get { return this.name; }
            set
            {
                if (base.Compare(name, value) != 0)
                {
                    this.name = value;
                    OnPropertyChanged("Name");
                }

            }
        }
        private string name;
        [XmlElement(ElementName = "Note")]
        public string NoteData
        {
            get { return this.noteData; }
            set
            {
                if (base.Compare(noteData, value) != 0)
                {
                    this.noteData = value; OnPropertyChanged("Note");
                }
            }
        }
        private string noteData;
        public Note()
        {
            Name = string.Empty;
            NoteData = string.Empty;

        }
        public Note(string name, string note)
        {
            Name = name;
            NoteData = note;
        }

    }

    public class NoteCollection : ObservableCollection<Note>
    {
    }
}
