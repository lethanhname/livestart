﻿using LiveStart.Models.Base;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;
using System.Xml.Serialization;

namespace LiveStart.Models
{
    public class Shortcut : BaseObject
    {
        //ID
        [XmlElement(ElementName = "ID")]
        public int ID
        {
            get { return this.id; }
            set
            {
                if (base.Compare(id, value) != 0)
                { this.id = value; OnPropertyChanged("ID"); }
            }
        }
        private int id;
        //Name
        [XmlElement(ElementName = "Group")]
        public string Group
        {
            get { return this.group; }
            set {
                if (base.Compare(group, value) != 0)
                { this.group = value; OnPropertyChanged("Group"); } }
        }
        private string group;
        //MaxWidth
        [XmlElement(ElementName = "MaxWidth")]
        public string MaxWidth
        {
            get { return this.maxWidth; }
            set
            {
                if (base.Compare(maxWidth, value) != 0)
                { this.maxWidth = value; OnPropertyChanged("MaxWidth"); }
            }
        }
        private string maxWidth;
        //Name
        [XmlElement(ElementName = "Name")]
        public string Name
        {
            get { return this.name; }
            set
            {
                if (base.Compare(name, value) != 0)
                { this.name = value; OnPropertyChanged("Name"); }
            }
        }
        [XmlIgnore]
        private string name;
        //Location
        [XmlElement(ElementName = "Location")]
        public string Location
        {
            get { return this.location; }
            set
            {
                if (base.Compare(location, value) != 0)
                { this.location = value; OnPropertyChanged("Location"); }
            }
        }
        [XmlIgnore]
        private string location;
        //Arguments
        [XmlElement(ElementName = "Arguments")]
        public string Arguments
        {
            get { return this.arguments; }
            set
            {
                if (base.Compare(arguments, value) != 0)
                { this.arguments = value; OnPropertyChanged("Arguments"); }
            }
        }
        [XmlIgnore]
        private string arguments;
        //ProgramType
        [XmlElement(ElementName = "ProgramType")]
        public string ProgramType
        {
            get { return this.programType; }
            set
            {
                if (base.Compare(programType, value) != 0)
                { this.programType = value; OnPropertyChanged("ProgramType"); }
            }
        }

        [XmlIgnore]
        private string programType;
        //IP
        [XmlElement(ElementName = "IP")]
        public string IP
        {
            get { return this.ip; }
            set
            {
                if (base.Compare(ip, value) != 0)
                { this.ip = value; OnPropertyChanged("IP"); }
            }
        }
        [XmlIgnore]
        private string ip;
        //UserName
        [XmlElement(ElementName = "UserName")]
        public string UserName
        {
            get { return this.userName; }
            set
            {
                if (base.Compare(userName, value) != 0)
                { this.userName = value; OnPropertyChanged("UserName"); }
            }
        }
        [XmlIgnore]
        private string userName;
        //Password
        [XmlElement(ElementName = "Password")]
        public string Password
        {
            get { return this.password; }
            set
            {
                if (base.Compare(password, value) != 0)
                { this.password = value; OnPropertyChanged("Password"); }
            }
        }

        [XmlIgnore]
        private string password;
        //Icon
        [XmlIgnore]
        public ImageSource Icon
        {
            get { return this.icon; }
            set
            {
                if (base.Compare(icon, value) != 0)
                { this.icon = value; OnPropertyChanged("Icon"); }
            }
        }
        [XmlIgnore]
        private ImageSource icon;
    }
    public class ShortcutCollection : ObservableCollection<Shortcut>
    {
    }
}
