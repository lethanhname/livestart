﻿using LiveStart.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;
using System.Xml.Serialization;

namespace LiveStart.Shortcuts
{
    public class ShortcutModel: ObservableObject
    {
        //Name
        [XmlElement(ElementName = "Group")]
        public string Group
        {
            get { return this.group; }
            set { this.group = value; OnPropertyChanged("Group"); }
        }
        private string group;
        //MaxWidth
        [XmlElement(ElementName = "MaxWidth")]
        public string MaxWidth
        {
            get { return this.maxWidth; }
            set { this.maxWidth = value; OnPropertyChanged("MaxWidth"); }
        }
        private string maxWidth;
        //Name
        [XmlElement(ElementName = "Name")]
        public string Name
        {
            get { return this.name; }
            set { this.name = value; OnPropertyChanged("Name"); }
        }
        [XmlIgnore]
        private string name;
        //Location
        [XmlElement(ElementName = "Location")]
        public string Location
        {
            get { return this.location; }
            set { this.location = value; OnPropertyChanged("Location"); }
        }
        [XmlIgnore]
        private string location;
        //Arguments
        [XmlElement(ElementName = "Arguments")]
        public string Arguments
        {
            get { return this.arguments; }
            set { this.arguments = value; OnPropertyChanged("Arguments"); }
        }
        [XmlIgnore]
        private string arguments;
        //ProgramType
        [XmlElement(ElementName = "ProgramType")]
        public string ProgramType
        {
            get { return this.programType; }
            set { this.programType = value; OnPropertyChanged("ProgramType"); }
        }

        [XmlIgnore]
        private string programType;
        //IP
        [XmlElement(ElementName = "IP")]
        public string IP
        {
            get { return this.ip; }
            set { this.ip = value; OnPropertyChanged("IP"); }
        }
        [XmlIgnore]
        private string ip;
        //UserName
        [XmlElement(ElementName = "UserName")]
        public string UserName
        {
            get { return this.userName; }
            set { this.userName = value; OnPropertyChanged("UserName"); }
        }
        [XmlIgnore]
        private string userName;
        //Password
        [XmlElement(ElementName = "Password")]
        public string Password
        {
            get { return this.password; }
            set { this.password = value; OnPropertyChanged("Password"); }
        }

        [XmlIgnore]
        private string password;
        //Icon
        [XmlIgnore]
        public ImageSource Icon
        {
            get { return this.icon; }
            set { this.icon = value; OnPropertyChanged("Icon"); }
        }
        [XmlIgnore]
        private ImageSource icon;
    }

    //[XmlTypeAttribute(AnonymousType = true)]
    //public class ShortcutModels : ObservableObject
    //{        
    //    //GroupData
    //    [XmlElement("Apps")]
    //    public ObservableCollection<ShortcutModel> Apps
    //    {
    //        get { return this.apps; }
    //        set { this.apps = value; OnPropertyChanged("Apps"); }
    //    }

    //    [XmlIgnore]
    //    private ObservableCollection<ShortcutModel> apps;

    //    public ShortcutModels()
    //    {
    //        Apps = new ObservableCollection<ShortcutModel>();
    //    }

    //}
}
