﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using LiveStart.Library;
using LiveStart.Shortcuts;
using System.Threading;
using System.ComponentModel;

namespace LiveStart
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : MahApps.Metro.Controls.MetroWindow
    {
        public MainWindow()
        {
            context = new MainViewModel();
            this.DataContext = context;
            InitializeComponent();
            context.LoadTheme();
            this.AllowsTransparency = true;
            CollectionView view = (CollectionView)CollectionViewSource.GetDefaultView(lbxApps.ItemsSource);
            PropertyGroupDescription groupDescription = new PropertyGroupDescription("Group");
            view.GroupDescriptions.Clear();
            view.GroupDescriptions.Add(groupDescription);
            context.PropertyChanged += Context_PropertyChanged;
        }

        private void Context_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if(e.PropertyName == "Refresh")
            {
                if(context.Refresh)
                {
                    refreshView();
                }                
            }
        }

        MainViewModel context;


        private void ListBoxItem_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ButtonState == MouseButtonState.Pressed && e.ChangedButton == MouseButton.Left)
            {
                ListBoxItem listBoxItem = sender as ListBoxItem;
                ShortcutModel data = (ShortcutModel)listBoxItem.Content;
                if (data != null)
                {                    
                    DragDrop.DoDragDrop(lbxApps, data, DragDropEffects.Move);
                }
            }
        }

        private void ListBoxItem_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            ListBoxItem listBoxItem = sender as ListBoxItem;
            ShortcutModel applicationModel = (ShortcutModel)listBoxItem.Content;
            if (applicationModel != null)
            {
                context.StartApplication(applicationModel);
            }
        }
        private void ListBoxItem_KeyDown(object sender, KeyEventArgs e)
        {
           
            if (context.CurrentShortcut != null)
            {
                if (e.Key == Key.Delete)
                {
                    context.Delete();
                }
                else if (e.Key == Key.Enter)
                {
                    context.StartApplication(context.CurrentShortcut);
                }
            }
        }

        private void lbxApps_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            context.IsNew = false;
        }

       
        private void dropApps(DragEventArgs e, ShortcutModel targetobj)
        {
            
        }

        private static object GetObjectDataFromPoint(ListBox source, Point point)
        {
            UIElement element = source.InputHitTest(point) as UIElement;
            if (element != null)
            {
                object data = DependencyProperty.UnsetValue;
                while (data == DependencyProperty.UnsetValue)
                {
                    data = source.ItemContainerGenerator.ItemFromContainer(element);
                    if (data == DependencyProperty.UnsetValue)
                        element = VisualTreeHelper.GetParent(element) as UIElement;
                    if (element == source)
                        return null;
                }
                if (data != DependencyProperty.UnsetValue)
                    return data;
            }

            return null;
        }
        
        private void gridNote_KeyDown(object sender, KeyEventArgs e)
        {            
            //if ((e.Key == Key.S) && (Keyboard.IsKeyDown(Key.LeftCtrl) || Keyboard.IsKeyDown(Key.RightCtrl)))
            //{
            //    MessageBox.Show("CTRL + V trapped");
            //}
        }

        private void btnNewRemote_Click(object sender, RoutedEventArgs e)
        {
            togProperties.IsChecked = true;
        }

        private void btnRefresh_Click(object sender, RoutedEventArgs e)
        {
            refreshView();
        }
        private void refreshView()
        {
            ICollectionView view = CollectionViewSource.GetDefaultView(lbxApps.ItemsSource);
            view.Refresh();
        }
        private void ListBoxGroup_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (e.LeftButton == MouseButtonState.Pressed)
            {
                GroupItem parent = sender as GroupItem;
                if (parent != null)
                {
                    if (parent.Content != null)
                    {
                        //object groupobj = GetObjectDataFromPoint(parent, e.GetPosition(parent));
                        CollectionViewGroup group = parent.Content as CollectionViewGroup;
                        if (group != null)
                        {
                            string groupName = group.Name.ToString();
                            DataObject data = new DataObject("Name", groupName);
                            DragDrop.DoDragDrop(parent, data, DragDropEffects.Move);
                        }
                    }
                }
            }
        }
        bool hasTarget = false;
        private void lbxApps_Drop(object sender, DragEventArgs e)
        {
            ShortcutModel targetobj = GetObjectDataFromPoint(lbxApps, e.GetPosition(lbxApps)) as ShortcutModel;
            if (!hasTarget)
            {
                if (e.Data.GetDataPresent(DataFormats.FileDrop))
                {

                    string[] files = (string[])e.Data.GetData(DataFormats.FileDrop);
                    foreach (string file in files)
                    {
                        string path = file;
                        string lnkDesc = string.Empty;
                        if (path.EndsWith(".lnk"))
                        {
                            path = WindowsShortcut.GetShortcutTargetFile(path, out lnkDesc);
                        }
                        if (path.EndsWith(".URL"))
                        {
                            lnkDesc = ".URL";
                            path = WindowsShortcut.GetInternetShortcut(path);
                        }
                        context.New(path, lnkDesc, targetobj);
                        context.Save();
                    }
                }
                else if (e.Data.GetDataPresent(DataFormats.Text))
                {
                    string html = e.Data.GetData(DataFormats.Text).ToString();
                    context.New(html, ProgramType.html, targetobj);
                    context.Save();
                }
                else
                {
                    ShortcutModel data = e.Data.GetData(typeof(ShortcutModel)) as ShortcutModel;
                    if (data != null)
                    {
                        if (data != targetobj)
                        {                            
                            context.ChangePosition(data, targetobj);
                            context.Save();
                            refreshView();
                        }
                    }
                }
            }
            hasTarget = false;
        }

        private void ListBoxGroup_Drop(object sender, DragEventArgs e)
        {
            GroupItem targetObject = sender as GroupItem;
            CollectionViewGroup group = targetObject.Content as CollectionViewGroup;
            string curgroupName = group.Name.ToString();
            if (e.Data.GetDataPresent("Name"))
            {                
                if (targetObject != null)
                {
                    if (targetObject.Content != null)
                    {
                        string groupName = e.Data.GetData("Name") as string;
                        if (groupName != curgroupName)
                        {
                            ShortcutModel shortcut = context.Apps.Where(p => p.Group == curgroupName).FirstOrDefault();
                            if(shortcut!=null)
                            {
                                IEnumerable<ShortcutModel> changeApps = context.Apps.Where(p => p.Group == groupName).ToList();
                                foreach (var item in changeApps)
                                {
                                    context.ChangePosition(item, shortcut);
                                }
                            }
                            context.Save();
                            refreshView();
                        }
                    }
                }
            }
            ShortcutModel targetobj = GetObjectDataFromPoint(lbxApps, e.GetPosition(lbxApps)) as ShortcutModel;
            if(targetobj!=null)
            {
                if (e.Data.GetDataPresent(DataFormats.FileDrop))
                {

                    string[] files = (string[])e.Data.GetData(DataFormats.FileDrop);
                    foreach (string file in files)
                    {
                        string path = file;
                        string lnkDesc = string.Empty;
                        if (path.EndsWith(".lnk"))
                        {
                            path = WindowsShortcut.GetShortcutTargetFile(path, out lnkDesc);
                        }
                        if (path.EndsWith(".URL"))
                        {
                            lnkDesc = ".URL";
                            path = WindowsShortcut.GetInternetShortcut(path);
                        }
                        context.New(path, lnkDesc, targetobj);
                        context.Save();
                    }
                }
                else if (e.Data.GetDataPresent(DataFormats.Text))
                {
                    string html = e.Data.GetData(DataFormats.Text).ToString();
                    context.New(html, ProgramType.html, targetobj);
                    context.Save();
                }
                else
                {

                    ShortcutModel data = e.Data.GetData(typeof(ShortcutModel)) as ShortcutModel;
                    if (data != null)
                    {
                        if (data != targetobj)
                        {
                            data.Group = curgroupName;
                            context.ChangePosition(data, targetobj);
                            context.Save();
                            refreshView();
                        }
                    }

                }
                hasTarget = true;
            }
            
        }

        private void myWindow_Closing(object sender, CancelEventArgs e)
        {
            context.SaveNote();
        }
    }
}
