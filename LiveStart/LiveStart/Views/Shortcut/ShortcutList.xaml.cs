﻿using LiveStart.Views.Note;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace LiveStart.Views.Shortcut
{
    /// <summary>
    /// Interaction logic for ShortcutList.xaml
    /// </summary>
    public partial class ShortcutList : Page
    {
        public ShortcutList()
        {
            InitializeComponent();
        }

        private void btnCreate_Click(object sender, RoutedEventArgs e)
        {
            this.NavigationService.Navigate(new ShortcutCreate());
        }

        private void btnNoteList_Click(object sender, RoutedEventArgs e)
        {
            this.NavigationService.Navigate(new NoteList());
        }
    }
}
