﻿using LiveStart.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace LiveStart.Notes
{
    public class NoteModel : ObservableObject
    {        

        [XmlElement(ElementName = "Name")]
        public string Name
        {
            get { return this.name; }
            set { this.name = value; OnPropertyChanged("Name"); }
        }
        private string name;
        [XmlElement(ElementName = "Note")]
        public string Note
        {
            get { return this.note; }
            set { this.note = value; OnPropertyChanged("Note"); }
        }
        private string note;
        public NoteModel()
        {
            Name = string.Empty;
            Note = string.Empty;

        }
        public NoteModel(string name, string note)
        {
            Name = name;
            Note = note;
        }

    }

}
