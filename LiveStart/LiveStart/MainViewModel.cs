﻿using LiveStart.Commands;
using LiveStart.Models;
using MahApps.Metro;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using MahApps.Metro.Controls;
using MahApps.Metro.Controls.Dialogs;
using System.Collections.ObjectModel;
using LiveStart.Shortcuts;
using System.IO;
using LiveStart.Library;
using System.Windows.Interop;
using System.Windows.Media.Imaging;
using System.Runtime.InteropServices;
using System.Drawing;
using System.ComponentModel;
using System.Diagnostics;
using LiveStart.Notes;
using System.Windows.Data;

namespace LiveStart
{
    public class MainViewModel: ObservableObject
    {
        #region Main
        public MainViewModel()
        {
            // create accent color menu items for the demo
            this.AccentColors = ThemeManager.Accents
                                            .Select(a => new AccentColorMenuData() { Name = a.Name, ColorBrush = a.Resources["AccentColorBrush"] as System.Windows.Media.Brush })
                                            .ToList();

            // create metro theme color menu items for the demo
            this.AppThemes = ThemeManager.AppThemes
                                           .Select(a => new AppThemeMenuData() { Name = a.Name, BorderColorBrush = a.Resources["BlackColorBrush"] as System.Windows.Media.Brush, ColorBrush = a.Resources["WhiteColorBrush"] as System.Windows.Media.Brush })
                                           .ToList();
            //
            string directory = System.IO.Path.GetDirectoryName(System.Diagnostics.Process.GetCurrentProcess().MainModule.FileName);
            appDataFile = directory + "\\AppData.xml";
            GetListApps();
            //note
            noteDataFile = directory + "\\NoteData.xml";
            GetListNotes();
            //
            IsNew = false;
        }
        
        public List<AccentColorMenuData> AccentColors { get; set; }
        public List<AppThemeMenuData> AppThemes { get; set; }

        #endregion

        #region Shortcut

        #region Properties
        private string appDataFile;
        private string noteDataFile;
        public bool IsNew;

        private bool refresh;
        public bool Refresh
        {
            get { return this.refresh; }
            set { this.refresh = value; OnPropertyChanged("Refresh"); }
        }

        private decimal windowsOpacity;
        public decimal WindowsOpacity
        {
            get { return this.windowsOpacity; }
            set { this.windowsOpacity = value; OnPropertyChanged("WindowsOpacity"); }
        }

        private ObservableCollection<ShortcutModel> apps;
        public ObservableCollection<ShortcutModel> Apps
        {
            get { return this.apps; }
            set { this.apps = value; OnPropertyChanged("Apps"); }
        }
        private ShortcutModel currentShortcut;
        public ShortcutModel CurrentShortcut
        {
            get { return this.currentShortcut; }
            set { this.currentShortcut = value; OnPropertyChanged("CurrentShortcut"); }
        }        
        #endregion

        #region Commands
        private ICommand newCommand;
        public ICommand NewCommand
        {
            get
            {
                if (newCommand == null)
                {
                    newCommand = new RelayCommand<object>(
                        param => New(),
                        param => true
                    );
                }
                return newCommand;
            }
        }

        private ICommand saveCommand;
        public ICommand SaveCommand
        {
            get
            {
                if (saveCommand == null)
                {
                    saveCommand = new RelayCommand<object>(
                        param => Save(),
                        param => (CurrentShortcut != null)
                    );
                }
                return saveCommand;
            }
        }
        private ICommand deleteCommand;
        public ICommand DeleteCommand
        {
            get
            {
                if (deleteCommand == null)
                {
                    deleteCommand = new RelayCommand<object>(
                        param => Delete(),
                        param => (CurrentShortcut != null)
                    );
                }
                return deleteCommand;
            }
        }
        private ICommand saveGroupCommand;
        public ICommand SaveGroupCommand
        {
            get
            {
                if (saveGroupCommand == null)
                {
                    saveGroupCommand = new RelayCommand<object>(
                        SaveGroup,
                        param => (CurrentShortcut != null)
                    );
                }
                return saveGroupCommand;
            }
        }
        
        private ICommand deleteGroupCommand;
        public ICommand DeleteGroupCommand
        {
            get
            {
                if (deleteGroupCommand == null)
                {
                    deleteGroupCommand = new RelayCommand<object>(
                        DeleteGroup,
                        param => (CurrentShortcut != null)
                    );
                }
                return deleteGroupCommand;
            }
        }
        #endregion

        #region Private Methods
        private void GetListApps()
        {
            apps = new ObservableCollection<ShortcutModel>();
            if (File.Exists(appDataFile))
            {
                apps = XMLSerializer.Deserialize(typeof(ObservableCollection<ShortcutModel>), appDataFile) as ObservableCollection<ShortcutModel>;
            }
            SetImage();
        }
        public void Save()
        {
            if (IsNew)
            {
                if (!string.IsNullOrEmpty(CurrentShortcut.Name))
                {
                    if (CurrentShortcut.ProgramType == ProgramType.Remote)
                    {
                        CurrentShortcut.Arguments = string.Format("/generic:TERMSRV/{0} /user:{1} /pass:{2}", currentShortcut.IP, currentShortcut.UserName, currentShortcut.Password);
                    }
                    SetItemImage(CurrentShortcut);
                    apps.Add(CurrentShortcut);
                }
            }
            
            IsNew = false;
            XMLSerializer.Serializer(typeof(ObservableCollection<ShortcutModel>), Apps, appDataFile);
            
            Refresh = false;
            Refresh = true;


        }
        private void New()
        {
            IsNew = true;
            string curGroup = "New Group";
            if(CurrentShortcut!=null)
            {
                curGroup = CurrentShortcut.Group;
            }
            CurrentShortcut = new ShortcutModel();
            CurrentShortcut.Group = curGroup;
            CurrentShortcut.ProgramType = ProgramType.Remote;
            CurrentShortcut.Location = @"c:\windows\system32\mstsc.exe";
        }
        public void New(string path, string type, ShortcutModel targetObject)
        {
            IsNew = true;
            CurrentShortcut = new ShortcutModel();
            if (System.IO.File.Exists(path))
            {

                CurrentShortcut.Name = System.IO.Path.GetFileNameWithoutExtension(path);                
                CurrentShortcut.Location = path;
                CurrentShortcut.ProgramType = ProgramType.File;
            }
            else if (Directory.Exists(path))
            {
                DirectoryInfo dirInfor = new DirectoryInfo(path);
                CurrentShortcut.Name = dirInfor.Name;
                CurrentShortcut.Location = dirInfor.FullName;
                CurrentShortcut.ProgramType = ProgramType.Directory;

            }
            else if (type==ProgramType.URL|| type == ProgramType.html)
            {
                CurrentShortcut.Name = path;
                CurrentShortcut.Location = path;
                CurrentShortcut.ProgramType = ProgramType.URL;
            }            
            if (CurrentShortcut.Location.Length > 0)
            {
                SetItemImage(CurrentShortcut);               
            }
            string curGroup = "New Group";  
            if (targetObject!=null)
            {
                curGroup = targetObject.Group;
            }
            CurrentShortcut.Group = curGroup;
            ChangePosition(CurrentShortcut, targetObject);
        }
        public void ChangePosition(ShortcutModel item, ShortcutModel targetItem)
        {
            if (item == null) return;
            if (targetItem == null)
            {
                string curGroup = "New Group";
                item.Group = curGroup;
            }
            else
            {
                int targetidx = Apps.IndexOf(targetItem);
                if (targetidx > -1 && targetidx < Apps.Count)
                {
                    if (Apps.Contains(item))
                    {
                        Apps.Remove(item);
                    }
                    Apps.Insert(targetidx, item);
                }
            }
            
        }
        public void SaveGroup(object items)
        {
            if(items!=null)
            {
                ReadOnlyObservableCollection<object> groups = items as ReadOnlyObservableCollection<object>;
                if (groups.Count > 1)
                {
                    for (int i = 1; i < groups.Count; i++)
                    {
                        ShortcutModel item = groups[i] as ShortcutModel;
                        item.Group = (groups[0] as ShortcutModel).Group;
                        item.MaxWidth = (groups[0] as ShortcutModel).MaxWidth;
                    }
                }
                             
                XMLSerializer.Serializer(typeof(ObservableCollection<ShortcutModel>), Apps, appDataFile);
                
            }
        }
        public async void DeleteGroup(object items)
        {
            if (items != null)
            {
                ReadOnlyObservableCollection<object> groups = items as ReadOnlyObservableCollection<object>;
                List<object> deletelst = groups.ToList();
                MetroWindow mainWindow = Application.Current.MainWindow as MetroWindow;
                var mySettings = new MetroDialogSettings()
                {
                    AffirmativeButtonText = "OK",
                    NegativeButtonText = "Go away!",
                    ColorScheme = mainWindow.MetroDialogOptions.ColorScheme
                };
                var first = deletelst[0] as ShortcutModel;
                MessageDialogResult result = await mainWindow.ShowMessageAsync("Delete (Y/N)?", "Delete "+ first.Group + " group!",
                    MessageDialogStyle.AffirmativeAndNegative, mySettings);
                if (result == MessageDialogResult.Affirmative)
                {    
                    foreach (var item in deletelst)
                    {
                        Apps.Remove(item as ShortcutModel);
                    }
                    
                    XMLSerializer.Serializer(typeof(ObservableCollection<ShortcutModel>), Apps, appDataFile);
                }
                Save();
                
            }
        }
        public async void Delete()
        {
            if (CurrentShortcut == null) return;
            MetroWindow mainWindow = Application.Current.MainWindow as MetroWindow;
            var mySettings = new MetroDialogSettings()
            {
                AffirmativeButtonText = "OK",
                NegativeButtonText = "Go away!",
                ColorScheme = mainWindow.MetroDialogOptions.ColorScheme
            };
            MessageDialogResult result = await mainWindow.ShowMessageAsync("Delete (Y/N)?", "Delete "+ CurrentShortcut .Name+ " shortcut!", 
                MessageDialogStyle.AffirmativeAndNegative, mySettings);
            if (result == MessageDialogResult.Affirmative)
            {
                Apps.Remove(CurrentShortcut);
                XMLSerializer.Serializer(typeof(ObservableCollection<ShortcutModel>), Apps, appDataFile);
            }
        }
        public void SetImage()
        {

            foreach (ShortcutModel item in Apps)
            {
                SetItemImage(item);
            }
        }
        public void SetItemImage(ShortcutModel item)
        {
            try
            {
                if (item.ProgramType == ProgramType.File || item.ProgramType == ProgramType.Remote)
                {
                    if (System.IO.File.Exists(item.Location))
                    {                        
                        Icon img = LiveStart.Library.WindowsIcons.GetLargeIcon(item.Location);
                        item.Icon = img.ToImageSource();
                    }

                }
                if (item.ProgramType == ProgramType.Directory)
                {
                    Icon img = LiveStart.Library.WindowsIcons.GetLargeFolderIcon();
                    item.Icon = img.ToImageSource();

                }
                if (item.ProgramType == ProgramType.html)
                {
                    Icon img = LiveStart.Library.WindowsIcons.GetLargeIconFromExtension(item.ProgramType);
                    item.Icon = img.ToImageSource();
                }
                if (item.ProgramType == ProgramType.URL)
                {
                    Icon img = LiveStart.Library.WindowsIcons.GetLargeIconFromExtension(item.ProgramType);
                    item.Icon = img.ToImageSource();
                }
            }
            catch (Exception)
            {
                //MessageBox.Show(item.Location);
            }



        }
        public void StartApplication(ShortcutModel applicationModel)
        {
            Process rdcProcess = new Process();
            switch (applicationModel.ProgramType)
            {
                case ProgramType.Remote:
                    ProcessStart.StartRemote(applicationModel.IP, applicationModel.Arguments);
                    break;
                default:
                    rdcProcess.StartInfo.FileName = applicationModel.Location;
                    rdcProcess.StartInfo.Arguments = applicationModel.Arguments;
                    rdcProcess.Start();
                    break;
            }
        }
        #endregion

        #endregion

        #region Notes
        private ObservableCollection<NoteModel> notes;
        public ObservableCollection<NoteModel> Notes
        {
            get { return this.notes; }
            set { this.notes = value; OnPropertyChanged("Notes"); }
        }
        private NoteModel currentNote;
        public NoteModel CurrentNote
        {
            get { return this.currentNote; }
            set { this.currentNote = value; OnPropertyChanged("CurrentNote"); }
        }
        #region Commands
        private ICommand newNoteCommand;
        public ICommand NewNoteCommand
        {
            get
            {
                if (newNoteCommand == null)
                {
                    newNoteCommand = new RelayCommand<object>(
                        param => NewNote(),
                        param => true
                    );
                }
                return newNoteCommand;
            }
        }

        private ICommand saveNoteCommand;
        public ICommand SaveNoteCommand
        {
            get
            {
                if (saveNoteCommand == null)
                {
                    saveNoteCommand = new RelayCommand<object>(
                        param => SaveNote(),
                        param => (CurrentNote != null)
                    );
                }
                return saveNoteCommand;
            }
        }
        private ICommand deleteNoteCommand;
        public ICommand DeleteNoteCommand
        {
            get
            {
                if (deleteNoteCommand == null)
                {
                    deleteNoteCommand = new RelayCommand<object>(
                        param => DeleteNote(),
                        param => (CurrentNote != null)
                    );
                }
                return deleteNoteCommand;
            }
        }
        #endregion
        #region Private Methods
        private void GetListNotes()
        {
            notes = new ObservableCollection<NoteModel>();
            if (File.Exists(noteDataFile))
            {
                notes = XMLSerializer.Deserialize(typeof(ObservableCollection<NoteModel>), noteDataFile) as ObservableCollection<NoteModel>;
            }
        }
        public void SaveNote()
        {            
            XMLSerializer.Serializer(typeof(ObservableCollection<NoteModel>), Notes, noteDataFile);
        }
        private void NewNote()
        {
            CurrentNote = new NoteModel("New Note", "");
            Notes.Add(CurrentNote);
        }
        
        private async void DeleteNote()
        {
            if (CurrentShortcut == null) return;
            MetroWindow mainWindow = Application.Current.MainWindow as MetroWindow;
            var mySettings = new MetroDialogSettings()
            {
                AffirmativeButtonText = "OK",
                NegativeButtonText = "Go away!",
                ColorScheme = mainWindow.MetroDialogOptions.ColorScheme
            };
            MessageDialogResult result = await mainWindow.ShowMessageAsync("Delete (Y/N)?", "Delete current note!",
                MessageDialogStyle.AffirmativeAndNegative, mySettings);
            if (result == MessageDialogResult.Affirmative)
            {
                if (CurrentNote != null)
                {
                    Notes.Remove(CurrentNote);
                    SaveNote();
                }
            }
        }
        #endregion
        #endregion
        private ICommand shutdownCommand;
        public ICommand ShutdownCommand
        {
            get
            {
                if (shutdownCommand == null)
                {
                    shutdownCommand = new RelayCommand<object>(
                        param => Shutdown(),
                        param => true
                    );
                }
                return shutdownCommand;
            }
        }
        private async void Shutdown()
        {
            if (CurrentShortcut == null) return;
            MetroWindow mainWindow = Application.Current.MainWindow as MetroWindow;
            var mySettings = new MetroDialogSettings()
            {
                AffirmativeButtonText = "OK",
                NegativeButtonText = "Go away!",
                ColorScheme = mainWindow.MetroDialogOptions.ColorScheme
            };
            MessageDialogResult result = await mainWindow.ShowMessageAsync("Shutdown Computer (Y/N)?", "Shutdown Computer!",
                MessageDialogStyle.AffirmativeAndNegative, mySettings);
            if (result == MessageDialogResult.Affirmative)
            {
                Application.Current.Shutdown();
                System.Diagnostics.Process.Start("Shutdown", "-a");
                string time = string.Format("-s -t {0}", 0);
                System.Diagnostics.Process.Start("Shutdown", time);
            }
        }
        private ICommand saveThemeCommand;
        public ICommand SaveThemeCommand
        {
            get
            {
                if (saveThemeCommand == null)
                {
                    saveThemeCommand = new RelayCommand<object>(
                        param => SaveTheme(),
                        param => true
                    );
                }
                return saveThemeCommand;
            }
        }
        private void SaveTheme()
        {
            var theme = ThemeManager.DetectAppStyle(Application.Current);
            Properties.Settings.Default.Theme = theme.Item1.Name;
            Properties.Settings.Default.Accent = theme.Item2.Name;
            Properties.Settings.Default.WindowsOpacity = WindowsOpacity;
            Properties.Settings.Default.Save();
        }
        public void LoadTheme()
        {
            string themeName = Properties.Settings.Default.Theme;
            string accentName = Properties.Settings.Default.Accent;
            WindowsOpacity = Properties.Settings.Default.WindowsOpacity;
            if (!String.IsNullOrEmpty(themeName) && !String.IsNullOrEmpty(accentName))
            {
                var appTheme = ThemeManager.GetAppTheme(themeName);
                var accent = ThemeManager.GetAccent(accentName);
                ThemeManager.ChangeAppStyle(Application.Current, accent, appTheme);
            }
        }
        #region Settings
        #endregion
    }
    public class ProgramType
    {
        public const string File = "File";
        public const string Directory = "Directory";
        public const string URL = ".URL";
        public const string html = ".html";
        public const string Remote = "Remote";

    }
    internal static class IconUtilities
    {
        [DllImport("gdi32.dll", SetLastError = true)]
        private static extern bool DeleteObject(IntPtr hObject);

        public static ImageSource ToImageSource(this Icon icon)
        {
            Bitmap bitmap = icon.ToBitmap();
            IntPtr hBitmap = bitmap.GetHbitmap();

            ImageSource wpfBitmap = Imaging.CreateBitmapSourceFromHBitmap(
                hBitmap,
                IntPtr.Zero,
                Int32Rect.Empty,
                BitmapSizeOptions.FromEmptyOptions());

            if (!DeleteObject(hBitmap))
            {
                throw new Win32Exception();
            }

            return wpfBitmap;
        }
    }
}
